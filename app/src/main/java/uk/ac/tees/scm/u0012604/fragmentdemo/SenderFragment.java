package uk.ac.tees.scm.u0012604.fragmentdemo;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SenderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SenderFragment extends Fragment {

    private MessageListener mMessageListener;

    public SenderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SenderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SenderFragment newInstance() {
        SenderFragment fragment = new SenderFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sender, container, false);

        Button btn = view.findViewById(R.id.sendMessageBtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageListener.onReceiveMessage("Hello, World! Sent from the " + SenderFragment.class.getSimpleName());
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof MessageListener) {
            mMessageListener = (MessageListener)context;
        }


    }

    public interface MessageListener {
        void onReceiveMessage(final String message);
    }

}
