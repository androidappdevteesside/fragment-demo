package uk.ac.tees.scm.u0012604.fragmentdemo;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onReceiveMessage(String message) {
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getFragmentManager().findFragmentById(R.id.receiverFragment);

        if(fragment != null) {
            fragment.updateText(message);
        }
    }
}
